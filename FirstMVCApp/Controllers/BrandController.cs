﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.DAL;
using FirstMVCApp.Models;
using FirstMVCApp.Models.Services;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            _brandService = brandService;
        }

        public IActionResult Index()
        {
            var Brands = _brandService.GetAllBrands().ToList();

            return View(Brands);
        }
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public IActionResult Create(BrandCreateModel model)
        {
            try
            {
                _brandService.CreateBrand(model);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ViewBag.BadRequestMessage = "This brand already exists ";
                return View("BadRequest");
            }
            
        }
    }
}