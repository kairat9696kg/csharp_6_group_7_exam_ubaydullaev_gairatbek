﻿using System;
using System.Linq;
using FirstMVCApp.Models;
using FirstMVCApp.Models.Services;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;


        public CategoryController(ICategoryService categoryService)
        {
            if (categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));

            _categoryService = categoryService;
        }

        public IActionResult Index()
        {
            var category = _categoryService.GetAllCategories().ToList();
            return View(category);
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(CategoryCreateModel model)
        {
            try
            {
                _categoryService.CreateCategory(model);
                return Redirect("Index");
            }
            catch
            {
                ViewBag.BadRequestMessage = $"This category already exists ";
                return View("BadRequest");
            }
        }
    }
}