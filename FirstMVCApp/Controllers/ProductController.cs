﻿using System;
using FirstMVCApp.Models;
using FirstMVCApp.Models.Services;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{

    public class ProductController : Controller
    {
        private readonly IProductService _productService;


        public ProductController(IProductService productService)
        {
            if (productService == null)
                throw new ArgumentNullException(nameof(productService));
            
            _productService = productService; 
            
        }

        [Route("Search/{name?}/{CategoryId?}/{BarndId?}/{priceFrom?}/{priceTo?}")]
        [Route("Product/Index")]
        public IActionResult Index([FromRoute]ProductFilterModel model)
        {
            try
            {
                var models = _productService.SearchProduct(model);

                return View(models);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult Create()
        {
            var model = _productService.GetProductCreateModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(ProductCreateModel model)
        {
            try
            {
                _productService.CreateProduct(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}