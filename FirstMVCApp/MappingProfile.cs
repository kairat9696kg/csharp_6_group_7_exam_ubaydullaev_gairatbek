﻿using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;

namespace FirstMVCApp
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateCategoryCreateModelToCategory();
            CreateCategoryToCategoryModel();
            CreateBrandToBrandsModel();
            CreateBrandCreateModelToBrand();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(pm => pm.BrandName,
                src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name ));
            CreateMap<Product, ProductModel>()
               .ForMember(pm => pm.CategoryName,
               src => src.MapFrom(p => p.Category.Name));
        }

        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }

        private void CreateCategoryToCategoryModel()
        {
            CreateMap<Category, CategoryModel>();
        }

        private void CreateCategoryCreateModelToCategory()
        {
            CreateMap<CategoryCreateModel, Category>();
        }

        private void CreateBrandToBrandsModel()
        {
            CreateMap<Brand, BrandModel>();
        }

        private void CreateBrandCreateModelToBrand()
        {
            CreateMap<BrandCreateModel, Brand>();
        }
    }
}
