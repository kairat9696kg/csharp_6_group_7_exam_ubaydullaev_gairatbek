﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models
{
    public class ProductFilterModel
    {
        public string Name { get; set; }
        public int? BrandId { get; set; }
        public int? CategoryId { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }
        public List<ProductModel> products { get; set; }
        public SelectList CategoriesList { get; set; }
        public SelectList BrandsList { get; set; }

    }
}
