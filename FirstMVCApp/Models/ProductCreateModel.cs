﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models
{
    public class ProductCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Наименование должно быть заполнынным")]
        public string Name { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Цена должна быть указана")]
        [Range(0, Double.PositiveInfinity,ErrorMessage = "Цена не может быть отрецательной")]
        public decimal Price { get; set; }

        [Display(Name = "Категория")]
        public int CategoryId { get; set; }

        [Display(Name = "Бренд")]
        public int? BrandId { get; set; }
        public SelectList CategoriesList { get; set; }
        public SelectList BrandsList { get; set; }
    }
}
