﻿using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FirstMVCApp.Models.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CategoryService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void CreateCategory(CategoryCreateModel model)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var categoriesList = uow.Categories.GetAll().ToList();
                Category category = Mapper.Map<Category>(model);
                if (categoriesList.Contains(category))
                    throw new Exception();
                uow.Categories.Create(category);
            }
        }

        public IEnumerable<CategoryModel> GetAllCategories()
        {
            using(var uow = _unitOfWorkFactory.Create())
            {
                var category = uow.Categories.GetAll().ToList();

                List<CategoryModel> categoryModel = Mapper.Map<List<CategoryModel>>(category);
                return categoryModel;
            }
        }
    }
}
