﻿using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models.Services
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BrandService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void CreateBrand(BrandCreateModel model)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var brands = uow.Brands.GetAll().ToList();
                Brand brand = Mapper.Map<Brand>(model);
                if (brands.Contains(brand))
                    throw new Exception();
                uow.Brands.Create(brand);
            }
        }

        public IEnumerable<BrandModel> GetAllBrands()
        {
            using(var uow = _unitOfWorkFactory.Create())
            {
                var Brands = uow.Brands.GetAll().ToList();
                List<BrandModel> brandModels = Mapper.Map<List<BrandModel>>(Brands);
                return brandModels;
            }
        }
    }
}
