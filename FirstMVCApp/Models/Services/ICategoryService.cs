﻿using System.Collections.Generic;

namespace FirstMVCApp.Models.Services
{
    public interface ICategoryService
    {
        public IEnumerable<CategoryModel> GetAllCategories();
        void CreateCategory(CategoryCreateModel model);
    }
}
