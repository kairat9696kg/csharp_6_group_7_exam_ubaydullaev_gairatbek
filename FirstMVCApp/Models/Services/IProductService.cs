﻿using System.Collections.Generic;

namespace FirstMVCApp.Models.Services
{
    public interface IProductService
    {
        public List<ProductModel> SearchProduct(ProductFilterModel model);
        ProductCreateModel GetProductCreateModel();
        void CreateProduct(ProductCreateModel model);
    }
}

