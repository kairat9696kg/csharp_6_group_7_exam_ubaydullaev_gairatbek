﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models
{
    public class BrandCreateModel
    {
        [Display (Name = "Наименование")]
        [Required (ErrorMessage = "Наименование должно быть заполнынным")]
        public string Name { get; set; }
    }
}
