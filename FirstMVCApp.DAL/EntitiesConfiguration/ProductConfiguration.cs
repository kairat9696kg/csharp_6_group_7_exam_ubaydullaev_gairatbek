﻿using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public class ProductConfiguration : BaseEntityConfiguration<Product>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Product> builder)
        {
            builder
               .HasOne(p => p.Category)
               .WithMany(p => p.products)
               .HasForeignKey(p => p.CategoryId)
               .IsRequired();

            builder
                .HasOne(p => p.Brand)
                .WithMany(p => p.products)
                .HasForeignKey(p => p.BrandId);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Product> builder)
        {
            builder
               .Property(p => p.Name)
               .HasMaxLength(500)
               .IsRequired();

            builder
                .Property(p => p.Price)
                .HasDefaultValue(0.0M)
                .IsRequired();

            builder
                .HasIndex(p => p.Name)
                .IsUnique();
        }
    }
}
