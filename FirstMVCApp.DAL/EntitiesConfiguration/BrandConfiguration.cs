﻿using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public class BrandConfiguration : BaseEntityConfiguration<Brand>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Brand> builder)
        {
            builder
                .HasMany(p => p.products)
                .WithOne(p => p.Brand)
                .HasForeignKey(p => p.BrandId);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Brand> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100);
                

            builder
                .HasIndex(b => b.Name)
                .IsUnique();

        }
    }
}
