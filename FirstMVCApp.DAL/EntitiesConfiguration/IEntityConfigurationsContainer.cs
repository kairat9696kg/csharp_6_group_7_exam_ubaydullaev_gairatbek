﻿using FirstMVCApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public interface IEntityConfigurationsContainer
    {
        IEntitiesConfiguration<Product> ProductConfiguration { get; }
        IEntitiesConfiguration<Category> CategoryConfiguration { get; }
        IEntitiesConfiguration<Brand> BrandConfiguration { get; }
    }

    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntitiesConfiguration<Product> ProductConfiguration { get; }
        public IEntitiesConfiguration<Category> CategoryConfiguration { get; }

        public IEntitiesConfiguration<Brand> BrandConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            ProductConfiguration = new ProductConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            BrandConfiguration = new BrandConfiguration();
        }
    }
}
