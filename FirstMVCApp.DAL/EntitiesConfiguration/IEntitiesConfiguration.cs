﻿using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public interface IEntitiesConfiguration<T> where T : Entity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
