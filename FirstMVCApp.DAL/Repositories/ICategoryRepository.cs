﻿using ConsoleAppWithDb.Repositories;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }

    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}
