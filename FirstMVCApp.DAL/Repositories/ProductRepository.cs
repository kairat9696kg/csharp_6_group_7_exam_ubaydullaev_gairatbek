﻿using System.Collections.Generic;
using System.Linq;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace ConsoleAppWithDb.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Products;
        }

        public IEnumerable<Product> GetAllWithCategoryAndBrands()
        {
            return entities
                .Include(e => e.Brand)
                .Include(e => e.Category)
                .ToList();
        }

        public IEnumerable<Product> GetAllWithCategoryAndBrandsByPrice(decimal priceFrom,decimal priceTo)
        {
            return
                 GetAllWithCategoryAndBrands()
                .Where(p => p.Price >= priceFrom && p.Price <= priceTo)
                .ToList();
                
        }

        public Product GetTheMostExpensiveProduct()
        {
            return entities.OrderByDescending(e => e.Price).First();
        }
    }
}