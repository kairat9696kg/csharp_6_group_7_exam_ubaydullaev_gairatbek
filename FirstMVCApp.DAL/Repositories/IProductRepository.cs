﻿using FirstMVCApp.DAL.Entities;
using System.Collections.Generic;

namespace ConsoleAppWithDb.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        Product GetTheMostExpensiveProduct();
        IEnumerable<Product> GetAllWithCategoryAndBrandsByPrice(decimal priceFrom,decimal priceTo);
        IEnumerable<Product> GetAllWithCategoryAndBrands();
    }
}
