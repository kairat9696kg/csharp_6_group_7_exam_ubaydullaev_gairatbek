﻿using ConsoleAppWithDb.Repositories;
using FirstMVCApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.Repositories
{
    public interface IBrandRepository : IRepository<Brand>
    {
    }
}
